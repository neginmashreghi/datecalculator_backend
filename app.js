
const express = require('express');
const bodyParser =require('body-parser');
var moment = require('moment');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  }
 

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.post('/add', function(req, res) {

    var randonDate = req.body.ranDate
    var numOfdays = req.body.num
    var newDate = new Date(randonDate)
    newDate.setDate(newDate.getDate() + numOfdays)
    var result = moment(randonDate).format('D MMMM Y') + " + " + numOfdays +" days = " + moment(newDate).format('D MMMM Y')
    res.json(result); 

});
app.post('/sub', function(req, res) {
   
    var randonDate = req.body.ranDate
    var numOfdays = req.body.num
    var newDate = new Date(randonDate)
    newDate.setDate(newDate.getDate() - numOfdays)
    var result = moment(randonDate).format('D MMMM Y') + " - " + numOfdays +" days = " + moment(newDate).format('D MMMM Y')
    res.json(result); 

});

app.post('/today', function(req, res) {
   
    var ranDate = randomDate(new Date(2012, 0, 1), new Date())
    var data = ranDate.getFullYear()+'-'+(ranDate.getMonth()+1)+'-'+ranDate.getDate()
    res.json(data);  

});



const port = 8080;
// eslint-disable-next-line no-console
app.listen(port, () => console.log(`Server started on port ${port}`)); 

module.exports = app;